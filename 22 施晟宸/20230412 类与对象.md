# 1.笔记

成员变量建议private私有化，只能在本类访问。

提供成套的setter和getter方法进行赋值和取值

this的作用（成员变量）：代表了当前对象的地址，可以访问当前对象的成员变量和成员方法。

如果你在这个类中定义了一个有参数构造器了，那么无参数构造器就消失了，最好有参无参都定义了

具体定义类的例子：

```java
public class Car {
    // 名词（属性，成员变量）
    String name;
    double price;

    // 动词（行为，成员方法）
    public void start(){
        System.out.println(name + "价格是" + price +"启动成功~~");
    }

    public void run(){
        System.out.println(name +"价格是" + price +"性能很好，跑的很快~~");
    }
}
```

# 2.作业

### 第一题：

```java
public class Employees {
    private int id;
    private String name;
    private int age;
    private double salary;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
```

对象

```java
public class testEmployees {
    public static void main(String[] args) {
        Employees employees = new Employees();
        employees.setName("张三");
        employees.setId(1);
        employees.setAge(23);
        employees.setSalary(10000);
        Employees employees1 = new Employees();
        employees1.setId(2);
        employees1.setName("李四");
        employees1.setAge(22);
        employees1.setSalary(11000);
        System.out.println("第1个员工的编号：" + employees.getId() + ",姓名:" + employees.getName() + ",年龄：" + employees.getAge() + ",薪资：" + employees.getSalary());
        System.out.println("第2个员工的编号：" + employees1.getId() + ",姓名:" + employees1.getName() + ",年龄：" + employees1.getAge() + ",薪资：" + employees1.getSalary());

    }
}
```

### 第二题：

```java
public class Employee {
    private int id;
    private String name;
    private int age;
    private double salary;

    public Employee() {
    }

    public Employee(int a, String b, int c, double d) {
        this.id = a;
        this.name = b;
        this.age = c;
        this.salary = d;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

//    public String toString(int id, String name, int age, double salary) {
//        return "编号为" + id + "的" + age + name + "薪水是" + salary;
//    }


}
```

对象

```java
public class testEmployee {
    public static void main(String[] args) {

        Employee employee = new Employee();
        employee.setId(1);
        employee.setName("战斗坤");
        employee.setAge(22);
        employee.setSalary(10000);
        System.out.println("编码" + employee.getId() + "员工名为" + employee.getName() + "的" + employee.getAge() + "岁的员工薪水为：" + employee.getSalary());


    }

}
```