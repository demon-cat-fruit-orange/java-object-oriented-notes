















```java
   
//我们计划为一个电器销售公司制作一套系统，公司的主要业务是销售一些家用电器，例如：电冰箱、洗衣机、电视机产品。
//冰箱类
//	属性：品牌、型号、颜色、售价、门款式、制冷方式

//洗衣机类
//	属性：品牌、型号、颜色、售价、电机类型、洗涤容量

//电视类
//	属性：品牌、型号、颜色、售价、屏幕尺寸、分辨率
//请根据上述类的设计，抽取父类“电器类”，并代码实现父类“电器类”、子类“冰箱类”，“洗衣机类”、“电视类”

类的设计为：
public class Household {
    String Brand;
    String Model;
    String Color;
    String Price;

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getColor() {
        return Color;
    }

    public void setColor(String color) {
        Color = color;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public Household(String brand, String model, String color, String price) {
        Brand = brand;
        Model = model;
        Color = color;
        Price = price;
    }

    public Household() {
    }

    @Override
    public String toString() {
        return "Household{" +
                "Brand='" + Brand + '\'' +
                ", Model='" + Model + '\'' +
                ", Color='" + Color + '\'' +
                ", Price='" + Price + '\'' +
                '}';
    }
}





public class Television extends Household {
    private String Screen_Size;
    private String Resolution_Ratio;

    public String getScreen_Size() {
        return Screen_Size;
    }

    public void setScreen_Size(String screen_Size) {
        Screen_Size = screen_Size;
    }

    public String getResolution_Ratio() {
        return Resolution_Ratio;
    }

    public void setResolution_Ratio(String resolution_Ratio) {
        Resolution_Ratio = resolution_Ratio;
    }

    public Television(String brand, String model, String color, String price, String screen_Size, String resolution_Ratio) {
        super(brand, model, color, price);
        Screen_Size = screen_Size;
        Resolution_Ratio = resolution_Ratio;
    }

    public Television(String screen_Size, String resolution_Ratio) {
        Screen_Size = screen_Size;
        Resolution_Ratio = resolution_Ratio;
    }


    public Television(String brand, String model, String color, String price) {
        super(brand, model, color, price);
    }

    public Television() {
    }

    @Override
    public String toString() {
        return "Television{" +
                "Screen_Size='" + Screen_Size + '\'' +
                ", Resolution_Ratio='" + Resolution_Ratio + '\'' +
                '}';
    }




}



 class Refrigerator extends Household {
     private String DoorStyle;
     private String Refrigerating_Method;

     public String getDoorStyle() {
         return DoorStyle;
     }

     public void setDoorStyle(String doorStyle) {
         DoorStyle = doorStyle;
     }

     public String getRefrigerating_Method() {
         return Refrigerating_Method;
     }

     public void setRefrigerating_Method(String refrigerating_Method) {
         Refrigerating_Method = refrigerating_Method;
     }

     public Refrigerator(String brand, String model, String color, String price, String doorStyle, String refrigerating_Method) {
         super(brand, model, color, price);
         DoorStyle = doorStyle;
         Refrigerating_Method = refrigerating_Method;
     }

     public Refrigerator(String doorStyle, String refrigerating_Method) {
         DoorStyle = doorStyle;
         Refrigerating_Method = refrigerating_Method;
     }

     public Refrigerator(String brand, String model, String color, String price) {
         super(brand, model, color, price);
     }

     public Refrigerator() {
     }

     @Override
     public String toString() {
         return "Refrigerator{" +
                 "DoorStyle='" + DoorStyle + '\'' +
                 ", Refrigerating_Method='" + Refrigerating_Method + '\'' +
                 '}';
     }
 }


public class Washing extends Household {
   private String Generator;
   private String Capacity;


    public String getGenerator() {
        return Generator;
    }

    public void setGenerator(String generator) {
        Generator = generator;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        Capacity = capacity;
    }


    public Washing(String brand, String model, String color, String price, String generator, String capacity) {
        super(brand, model, color, price);
        Generator = generator;
        Capacity = capacity;
    }

    public Washing(String generator, String capacity) {
        Generator = generator;
        Capacity = capacity;
    }

    public Washing(String brand, String model, String color, String price) {
        super(brand, model, color, price);
    }

    public Washing() {
    }

    @Override
    public String toString() {
        return "Washing{" +
                "Generator='" + Generator + '\'' +
                ", Capacity='" + Capacity + '\'' +
                '}';
    }



}







```









```java
//我们计划为一个动物园制作一套信息管理系统，根据与甲方沟通，我们归纳了有以下几种动物需要记录到系统中：
//请根据以上需求，使用“继承”设计出三层的类结构
|--动物类
    |--鸟类：
        |--鹦鹉类
        |--猫头鹰类
        |--喜鹊类
  
    |--哺乳类：
        |--大象类
        |--狼类
        |--长颈鹿类
  
    |--爬行类：
        |--鳄鱼类
        |--蛇类
        |--乌龟类

作为父类的类都应该定义一些共性内容，每种子类也都有一些特定的内容。 重点是类的层次结构，类成员简单表示即可。


public class Animal {
        String type;
        String name;
        String sex;

        public void print(String name) {
            System.out.println(name + "是一只动物");
        }

        public void eat(String name) {
            System.out.println(name + "会觅食");
        }
    }



public class Bird extends Animal {
    String wing;
    String color;

    public void fly(String name) {
        System.out.println(name + "会飞");
    }

    public void leyEggs(String name) {
        System.out.println(name + "会下蛋");
    }
}

class Parrot extends Bird {
    public void say(String name) {
        System.out.println(name + "会说话");
    }
}
class Owl{
    public void stayUpLate(String name){
        System.out.println(name + "会熬夜");
    }

}
class Magpie{
    public void robber(String name){
        System.out.println(name +"会抢别人的家");
    }
}





public class Crawl extends Animal{// 爬行类
    public void temperature(String name){
        System.out.println(name+"体温不恒定");
    }
}
class Crocodile extends Crawl{// 鳄鱼类
    public void roll(String name){
        System.out.println(name+"会使用爱的魔力转圈圈");
    }
}
class Snake extends Crawl{// 蛇类
    public void stealth(String name){
        System.out.println(name+"会隐身");
    }
}
class Turtle extends Crawl{// 乌龟类
    public void speed(String name){
        System.out.println(name+"的速度很慢");
    }
}



public class Lactation extends Animal{// 哺乳类
    public void vivi(String name){
        System.out.println(name+"是胎生的");
    }
}
class Elephant extends Lactation{// 大象类
    public void nose(String name){
        System.out.println(name+"的鼻子很长");
    }
}
class Wolf extends Lactation{// 狼类
    public void social(String name){
        System.out.println(name+"是群居动物");
    }
}
class Giraffe extends Lactation{// 长颈鹿类
    public void neck(String name){
        System.out.println(name+"的脖子很长");
    }
}
```

